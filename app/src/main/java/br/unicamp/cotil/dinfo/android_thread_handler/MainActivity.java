package br.unicamp.cotil.dinfo.android_thread_handler;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
    private TextView txtStatus;
    private Button btnProcessar;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtStatus = (TextView) findViewById(R.id.txtStatus);
        btnProcessar=(Button) findViewById(R.id.btnProcessar);
    }


    public void processar(View v){
        txtStatus.setText("Processando...");
        btnProcessar.setEnabled(false);
        executaAlgoDemorado();

    }

    private void executaAlgoDemorado(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                SystemClock.sleep(10000); //dorme por 10 segundos
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        txtStatus.setText("Processamento finalizado!");
//                        btnProcessar.setEnabled(true);
//                    }
//                });

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        txtStatus.setText("Processamento finalizado!");
                        btnProcessar.setEnabled(true);
                    }
                }) ;
            }
        }).start();
    }
}
